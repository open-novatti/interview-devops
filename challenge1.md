
1. Allow your shell script to accept 3 arguments, action, env
assume the name of shell script is command.sh, you should be able to use it as follows:
./command.sh [action] [env]
In your script, you need to be able to do a simple condition check. 
If the [env] is "dev", you need to echo the following text:
This is a dev environment, we only plan for fun! 
Else if [env] is "prod", you need to echo the following text:
This is a production environment, we will apply the action seriously!

Requirements:
Clear logic in the script and easy to maintain
Quit the shell/execution if there is any error



